package org.fast.crawler.demo.entity.dao;

import org.fast.crawler.demo.entity.support.BaseDao;
import org.fast.crawler.demo.entity.CrawlKeyWord;
import org.springframework.stereotype.Repository;

/**
 * Created by xp017734 on 10/10/15.
 */
@Repository
public class CrawlKeyWordDao extends BaseDao<CrawlKeyWord> {

}
