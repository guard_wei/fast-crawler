package org.fast.crawler.demo.entity.dao;

import org.fast.crawler.demo.entity.support.BaseDao;
import org.fast.crawler.demo.entity.CrawlSiteConfig;
import org.springframework.stereotype.Repository;

/**
 * Created by xp017734 on 10/10/15.
 */
@Repository
public class CrawlSiteConfigDao extends BaseDao<CrawlSiteConfig> {
}
